# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression
import statistics

def regression(hour_pivot):

   
    year_begin = 1998
    year_end = 2020
    relevant_years = list(range(year_begin,year_end+1))
    backward = list(reversed(range(year_begin,year_end+1)))

    
    
    for code in hour_pivot.index.get_level_values(0).unique() :
        first_consecutive_values={}
        last_consecutive_values={}
        first_values =[1,2,3]   
        last_values = [4,5,6]  
        print(code)
            

        for sex in hour_pivot.index.get_level_values(1).unique():
            for classif in hour_pivot.index.get_level_values(2).unique():

                for years in relevant_years:

                    if  not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]].isnull().values.all():
                        first_year = years
                        break
                    else:
                        first_year= None
                        
                for years in backward:
                    if  not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]].isnull().values.all():
                        last_year = years
                        break
                    else :
                        last_year= None
                        

               
                if first_year == year_begin and last_year == year_end:
                    continue
                                                                        
                for years in backward:
                    if  not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]].isnull().values.all():
                        value =hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]
                        value=float(value.to_string(index=False, header=False))
                        year_zero = years
                        if value == 0 :
                            for years in list(range(year_zero,year_end+1)):   
                                hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]] = 0
                
                if  (first_year == None and last_year == None):
                   continue
                
                if first_year == last_year:
                    value =hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year]]
                    value=float(value.to_string(index=False, header=False))
                    for years  in list(range(year_begin,year_end+1)):   
                        hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]] = value

                    continue
                pass
         
                if last_year-first_year==1 :
                    valeur=0
                    for years in range (first_year, last_year+1):
                        instant=hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]
                        instant=float(instant.to_string(index=False, header=False))
                        valeur=valeur+instant
                    valeur= valeur // 2
                    
                    
                    for years in range (year_begin,first_year):
                        if hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]].isnull().values.all():
                            hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=valeur

                    for years in range (last_year,year_end+1):
                        if hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]].isnull().values.all():
                            hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=valeur
                    
                    continue
                
                pass
                    

                   
                if last_year-first_year<5 and last_year-first_year>=2:
                    for years in range(first_year-1,year_begin-1,-1):
                        for num in first_values:
                            value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years+num]]
                            value_num=float(value_num.to_string(index=False, header=False))
                            first_consecutive_values[(num)]=value_num  
                        numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                        average = statistics.mean(numbers1)
                        hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                    first_consecutive_values={}

                    for years in range(last_year+1,year_end+1):
                        for num in first_values:
                            value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years-num]]
                            value_num=float(value_num.to_string(index=False, header=False))
                            first_consecutive_values[(num)]=value_num 
                                
                                
                        numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                        average = statistics.mean(numbers1)
                        hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                    first_consecutive_values={}
                    
                       
                    
                    continue
                pass                                    
                
                

                
                if not first_year == year_begin and last_year == year_end:
                    if not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_end]].isnull().values.all():
                        x = np.array([1, 2, 3]).reshape((-1, 1))
                        begin = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+2]]).to_string(index=False,header=False))])
                        end = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_end-2]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_end-1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_end]]).to_string(index=False,header=False))])
                        model = LinearRegression().fit(x, begin) 
                        model2 = LinearRegression().fit(x, end)
                        if  np.sign(model.coef_) == np.sign(model2.coef_):
                            for num in first_values:
                                value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+num-1]]
                                value_num=float(value_num.to_string(index=False, header=False))
                                first_consecutive_values[(num)]=value_num  
                            for num in last_values :
                                value_num=hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_end+4-num]]
                                value_num=float(value_num.to_string(index=False, header=False))             
                                last_consecutive_values[(num)]=value_num


                            numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                            average = statistics.mean(numbers1)
                            
                            numbers2 = [last_consecutive_values[key] for key in last_consecutive_values]
                            average2 = statistics.mean(numbers2)

                            x = np.array([first_year+1, year_end-1]).reshape((-1, 1))
                            linear = np.array([average, average2])
                            model3=LinearRegression().fit(x, linear)
                            for years in range(first_year-1,year_begin-1,-1):
                                if model3.coef_ * years +model3.intercept_ >= 0 :
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=model3.coef_ * years +model3.intercept_
                                else :
                                    previous_value = hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years+1]]
                                    previous_value = float(previous_value.to_string(index=False, header=False))
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]= previous_value                                   
                            first_consecutive_values={}
                            last_consecutive_values={}
                                
                        else :
                            for years in range(first_year-1,year_begin-1,-1):
                                for num in first_values:
                                    value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years+num]]
                                    value_num=float(value_num.to_string(index=False, header=False))
                                    first_consecutive_values[(num)]=value_num  
                            
                                numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                                average = statistics.mean(numbers1)
                                
                                hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                                first_consecutive_values={}
                            

                
                if  first_year == year_begin and not last_year == year_end:
                    if not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_begin]].isnull().values.all():
                        x = np.array([1, 2, 3]).reshape((-1, 1))
                        begin = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_begin]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_begin+1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_begin+2]]).to_string(index=False,header=False))])
                        end = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year-2]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year-1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year]]).to_string(index=False,header=False))])
                        
                        model = LinearRegression().fit(x, begin) 
                        model2 = LinearRegression().fit(x, end)

                        if  np.sign(model.coef_) == np.sign(model2.coef_):
                            for num in first_values:
                                value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[year_begin+num-1]]
                                value_num=float(value_num.to_string(index=False, header=False))
                                first_consecutive_values[(num)]=value_num  
                            for num in last_values :
                                value_num=hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year+4-num]]
                                value_num=float(value_num.to_string(index=False, header=False))             
                                last_consecutive_values[(num)]=value_num


                            numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                            average = statistics.mean(numbers1)
                            
                            numbers2 = [last_consecutive_values[key] for key in last_consecutive_values]
                            average2 = statistics.mean(numbers2)

                            x = np.array([year_begin+1, last_year-1]).reshape((-1, 1))
                            linear = np.array([average, average2])
                            model3=LinearRegression().fit(x, linear)
                            for years in range(last_year+1,year_end+1):
                                if model3.coef_ * years +model3.intercept_ >= 0 :
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=model3.coef_ * years +model3.intercept_
                                else :
                                    previous_value = hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years-1]]
                                    previous_value = float(previous_value.to_string(index=False, header=False))
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]= previous_value                                        
                            first_consecutive_values={}
                            last_consecutive_values={}
    
                        else :
                            for years in range(last_year+1,year_end+1):
                                for num in first_values:
                                    value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years-num]]
                                    value_num=float(value_num.to_string(index=False, header=False))
                                    first_consecutive_values[(num)]=value_num  
                           


                                numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                                average = statistics.mean(numbers1)
                                hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                                first_consecutive_values={}
                            
 
                                                                        
                
                if not first_year == year_begin and not last_year == year_end:
                    if not hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year]].isnull().values.all():
                        x = np.array([1, 2, 3]).reshape((-1, 1))
                        begin = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+2]]).to_string(index=False,header=False))])
                        end = np.array([float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year-1]]).to_string(index=False,header=False)), float((hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year-2]]).to_string(index=False,header=False))])
                        model = LinearRegression().fit(x, begin) 
                        model2 = LinearRegression().fit(x, end)
                        if  np.sign(model.coef_) == np.sign(model2.coef_):
                            for num in first_values:
                                value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[first_year+num-1]]
                                value_num=float(value_num.to_string(index=False, header=False))
                                first_consecutive_values[(num)]=value_num  
                            for num in last_values :
                                value_num=hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[last_year+4-num]]
                                value_num=float(value_num.to_string(index=False, header=False))             
                                last_consecutive_values[(num)]=value_num


                            numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                            average = statistics.mean(numbers1)
                            
                            numbers2 = [last_consecutive_values[key] for key in last_consecutive_values]
                            average2 = statistics.mean(numbers2)

                            x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                            linear = np.array([average, average2])
                            model3=LinearRegression().fit(x, linear)
                            #print(first_year)
                            print(year_begin)
                            for years in range(first_year-1,year_begin-1,-1):
                                if model3.coef_ * years +model3.intercept_ >= 0 :
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=model3.coef_ * years +model3.intercept_
                                else :
                                    previous_value = hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years+1]]
                                    previous_value = float(previous_value.to_string(index=False, header=False))
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]= previous_value              
                            for years in range(last_year+1,year_end+1):
                                if model3.coef_ * years +model3.intercept_ >= 0 :
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=model3.coef_ * years +model3.intercept_
                                else :
                                    previous_value = hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years-1]]
                                    previous_value = float(previous_value.to_string(index=False, header=False))
                                    hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]= previous_value                                   
                            first_consecutive_values={}
                            last_consecutive_values={}
                                
                        else :
                            for years in range(first_year-1,year_begin-1,-1):
                                for num in first_values:
                                    value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years+num]]
                                    value_num=float(value_num.to_string(index=False, header=False))
                                    first_consecutive_values[(num)]=value_num  

                                numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                                average = statistics.mean(numbers1)
                            
                                hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                                first_consecutive_values={}
                            
                            for years in range(last_year+1,year_end+1):
                                for num in first_values:
                                    value_num= hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years-num]]
                                    value_num=float(value_num.to_string(index=False, header=False))
                                    first_consecutive_values[(num)]=value_num  
                            

                                numbers1 = [first_consecutive_values[key] for key in first_consecutive_values]
                                average = statistics.mean(numbers1)

                                hour_pivot.loc[((hour_pivot.index.get_level_values(0)==code)&(hour_pivot.index.get_level_values(1)==sex)&(hour_pivot.index.get_level_values(2)==classif)),[years]]=average
                                first_consecutive_values={}
   
    
    hour_pivot.to_csv('regression.csv',index = False)
                            
    table_of_interest = hour_pivot.copy()
    return table_of_interest 