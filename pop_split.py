import pandas as pd
import country_converter as coco
import json
from pathlib import Path
from datetime import datetime 
#from pathlib import Path
import ilo_download
from regression import regression
from string import digits

'''
Download data from url and store in download folder
Unzip and store the data in the data folder
'''

src_url = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/EMP_2EMP_SEX_ECO_NB_A.csv.gz"
src_csv = Path("EMP_2EMP_SEX_ECO_NB_A.csv")

src_url2 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/HOW_TEMP_SEX_ECO_NB_A.csv.gz"
src_csv2 = Path("HOW_TEMP_SEX_ECO_NB_A.csv")

storage_root = Path(".").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"

data_gz = ilo_download.download_data(src_url=src_url, storage_path=download_path)
ilo_download.create_archive(src_csv=src_csv,storage_path=data_path)
ilo_download.extract_data(download_path,data_path,src_csv,src_url)


data_gz = ilo_download.download_data(src_url=src_url2, storage_path=download_path)
ilo_download.create_archive(src_csv=src_csv2,storage_path=data_path)
ilo_download.extract_data(download_path,data_path,src_csv2,src_url2)


'''
We read the downloaded data and add to it the column ref_area.label which correspond to the name_short designation in country converter
'''

data_list = pd.read_csv(data_path/src_csv, encoding="utf-8-sig") 
hour_list = pd.read_csv(data_path/src_csv2, encoding="utf-8-sig") 

add_ref_area_label = pd.read_csv('aux/EMP_2EMP_SEX_ECO_NB_A-full-2021-11-30.csv', encoding="utf-8-sig") 
data_list.insert(1, 'ref_area.label' ,'')

for ref in add_ref_area_label['ref_area'].unique():
    data_list.loc[data_list['ref_area']==ref,['ref_area.label']]=add_ref_area_label.loc[add_ref_area_label['ref_area']==ref,['ref_area.label']]    



'''
We have the list of the missing coutries in ILO table
'''

xls = pd.ExcelFile('aux/Exiobase_Population_Data_not_found.xlsx')
missing_data = pd.read_excel(xls, 'Exiobase data not automatised')
missing_data.columns = missing_data.iloc[missing_data[missing_data.values=='ISO3'].index.values[0]]

'''
In ILO table, the ISO3 associated to channel island is CHA. However, in coco CHI is allocated to this location.
We replace CHA bu CHI in ILO table
'''

data_list["ref_area"].replace({"CHA": "CHI"}, inplace=True)

'''
We add the EXIO3 region for each ISO3 countries 
'''

country_code = list(data_list['ref_area'])
cc_all = coco.CountryConverter(include_obsolete=True)
data_list.insert(2, 'EXIO3', cc_all.convert(names = country_code,src="ISO3", to='EXIO3'))



#country_not_found = ['Channel Islands']
#add_data = pd.DataFrame.from_dict({
#       'name_short' : ['Channel Islands'],
#       'name_official' : ['Channel Islands'],
#       'regex' : ['channel.?island.*'], 
#       'EXIO3': ['WE'],
#       'ISO3': ['CHI']}
#)
#extended_converter = coco.CountryConverter(additional_data=add_data)
#extended_converter.convert(country_not_found, src='name_short', to='EXIO3')
#extended_converter.convert(country_not_found, src='name_short', to='ISO3')
#
#REGARDING CHA in ILO table, this corresponf ot CHI in coco converter.
#We have to hange CHA by CHI.


#'''
#Here we add the EXIO3 and the new ISO3 for channel Island (ISO3 is in coco CHI but was CHA in ILO file)
#'''
#data_list.loc[data_list['ref_area.label']=='Channel Islands',['EXIO3']]=extended_converter.convert('Channel Islands', src='name_short',to='EXIO3')
#data_list.loc[data_list['ref_area.label']=='Channel Islands',['ref_area']]=extended_converter.convert('Channel Islands', src='name_short',to='ISO3')



#data_list.insert(1, 'ref_area.label', cc_all.convert(names = country_code,src="ISO3", to='name_short'))



'''
Create a list of ISO3 available in ILO
'''

ISO3_in_ILO=[item for item in list(data_list['ref_area'].unique()) if  item.isalpha()]
ISO3_in_EXIOBASE = cc_all.ISO3.ISO3


'''
We compare the list of ISO3 available in the ILO table and the ISO3 in EXIOBASE
We then create a list with the missing coutries in ILO table
''' 
missing_countries = []
for item in ISO3_in_EXIOBASE:
    if not item in ISO3_in_ILO:
        missing_countries.append(item)
        
        
'''
classifications
'''
classifications = list(data_list['classif1'].unique())


data_list_old = data_list.copy()
data_list = data_list.drop(['source','indicator'], axis = 1)

list_sex = list(data_list['sex'].unique())

'''
Some of the missing data are available in CIA
'''
with open("aux/CIA.json", "r") as read_file:
    data_cia = json.load(read_file)


'''
Add missing countries in data_list
'''



name_missing_official = []
name_short = []
for item in missing_countries:
    name_missing=cc_all.convert(names = item,src = 'ISO3',to='name_official')
    short=cc_all.convert(names = item,src = 'ISO3',to='name_short')
    
    name_missing_official.append(name_missing) 
    name_short.append(short)


'''
Search for population and date in CIA for missing countries
Create a df fetch_data with EXI3, population and date as columns
'''

fetched_data = pd.DataFrame(data=None,columns=['ISO3','population','date'])
for a in data_cia['countries'].keys():

    name_short_cia = cc_all.convert(names = a ,src = 'regex',to='ISO3')
    print(a)
    for item in missing_countries:
        name_missing=item
        if name_missing == name_short_cia:
            print(name_missing)
            if data_cia['countries'].get(a).get('data').get('economy').get('labor_force') is not None :
                if 'total_size' in data_cia['countries'].get(a).get('data').get('economy').get('labor_force').keys():
                    print(a,data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('total_people'),data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('date'))
                    fetched_data = fetched_data.append(pd.Series([item,data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('total_people'),data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('date')],index=['ISO3','population','date']),ignore_index=True)
                    continue
                else:
                    continue
            else :
                continue
        else :
            continue
        
'''
Make sure only the year appear in fetched_data['date'] ( not date and month )
'''

        
for item in missing_countries:
    if item in fetched_data['ISO3'].values :
        print(item)
        of_interest = fetched_data.loc[fetched_data['ISO3']==item,['date']].to_string(index=False, header=False)
        print(of_interest)
        if len(str(of_interest))!=4:
            date_object = datetime.strptime(of_interest,'%Y-%m-%d')
            date_object_year=date_object.strftime('%Y')
            print(item,date_object_year)
            fetched_data.loc[fetched_data['ISO3']==item,['date']]=date_object_year
        else :
            continue

data_list = data_list[~data_list['EXIO3'].str.contains("not found")]
#data_list = data_list[~data_list['ref_area'].str.contains(r'\d+')]
        
        
column_data_list = []
for i in data_list.columns:
    print(i)
    column_data_list.append(i)


filename = Path('aux/countries_en.csv')
df = pd.read_csv(filename)
from_cia_to_ilo =  pd.DataFrame(data=None,columns=column_data_list)


 
for a in missing_countries:
    if a in fetched_data['ISO3'].values:
        print(a)

        ILO_region = ILO_Subregion_Broad  = None
    
        ILO_region = df.loc[df['ISO3 Code']==a,['ILO Region']]
        ILO_region = ILO_region.to_string(index=False, header=False)
        ILO_Subregion_Broad = df.loc[df['ISO3 Code']==a,['ILO Subregion - Broad']]
        ILO_Subregion_Broad = ILO_Subregion_Broad.to_string(index=False, header=False)
        
        World_Bank_Income_Group = df.loc[df['ISO3 Code']==a,['World Bank Income Group']]
        World_Bank_Income_Group = World_Bank_Income_Group.to_string(index=False, header=False)  
        
        for s in list_sex:
            for c in classifications :
                if str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group) in data_list_old['ref_area.label'].values:
                    date = int(fetched_data.loc[fetched_data['ISO3']==a,['date']].to_string(index=False, header=False))
                    pop_known_year_know = float(fetched_data.loc[fetched_data['ISO3']==a,['population']].to_string(index=False, header=False))
                    
                    '''
                    case of TCA, population unknown for the corresponding ILO_Subregion_Broad in  1990.
                    we have to extrapolate known values.
                    We know the population of the ILO_Subregion_Broad for 1991 and  1992. we deduced from these the value for 1990.
                    '''
                    if date <=1990 :
                        pop_total_year_know_1991 = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']=='ECO_SECTOR_TOTAL')&(data_list_old['sex']=='SEX_T')&(data_list_old['time']==1991),['obs_value']].to_string(index=False, header=False)))
                        pop_total_year_know_1992 = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']=='ECO_SECTOR_TOTAL')&(data_list_old['sex']=='SEX_T')&(data_list_old['time']==1992),['obs_value']].to_string(index=False, header=False)))
                        pop_total_year_know = 2*pop_total_year_know_1991-pop_total_year_know_1992
                    
                    else :
                        
                        pop_total_year_know = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']=='ECO_SECTOR_TOTAL')&(data_list_old['sex']=='SEX_T')&(data_list_old['time']==date),['obs_value']].to_string(index=False, header=False)))
                    
                    for year in range(1991,2020):
                        if year == int(fetched_data.loc[fetched_data['ISO3']==a,['date']].to_string(index=False, header=False)):
                            pop_of_interest = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']==c)&(data_list_old['sex']==s)&(data_list_old['time']==year),['obs_value']].to_string(index=False, header=False)))
                            from_cia_to_ilo=from_cia_to_ilo.append(pd.Series([a,cc_all.convert(names = a ,src = 'ISO3',to='name_official'),cc_all.convert(names = a ,src = 'ISO3',to='EXIO3'),s,c,year,pop_known_year_know/pop_total_year_know*pop_of_interest,'ILO_Subregion_Broad'], index=[i for i in column_data_list]),ignore_index=True)
                        else:

                            pop_of_interest = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_Subregion_Broad)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']==c)&(data_list_old['sex']==s)&(data_list_old['time']==year),['obs_value']].to_string(index=False, header=False)))
                            pop_country=pop_known_year_know*pop_of_interest/pop_total_year_know
                            from_cia_to_ilo=from_cia_to_ilo.append(pd.Series([a,cc_all.convert(names = a ,src = 'ISO3',to='name_official'),cc_all.convert(names = a ,src = 'ISO3',to='EXIO3'),s,c,year,pop_country,'ILO_Subregion_Broad'], index=[i for i in column_data_list]),ignore_index=True)
    
            
  
                else :
                    if str(ILO_region)+': '+str(World_Bank_Income_Group) in data_list_old['ref_area.label'].values:
                        date = int(fetched_data.loc[fetched_data['ISO3']==a,['date']].to_string(index=False, header=False))
                        pop_total_year_know = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_region)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']=='ECO_SECTOR_TOTAL')&(data_list_old['sex']=='SEX_T')&(data_list_old['time']==date),['obs_value']].to_string(index=False, header=False)))
                        pop_known_year_know = float(fetched_data.loc[fetched_data['ISO3']==a,['population']].to_string(index=False, header=False))
                        for year in range(1991,2020):
                            if year == int(fetched_data.loc[fetched_data['ISO3']==a,['date']].to_string(index=False, header=False)):
                                pop_of_interest = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_region)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']==c)&(data_list_old['sex']==s)&(data_list_old['time']==year),['obs_value']].to_string(index=False, header=False)))
                                from_cia_to_ilo=from_cia_to_ilo.append(pd.Series([a,cc_all.convert(names = a ,src = 'ISO3',to='name_official'),cc_all.convert(names = a ,src = 'ISO3',to='EXIO3'),s,c,year,pop_known_year_know/pop_total_year_know*pop_of_interest,'ILO_region'], index=[i for i in column_data_list]),ignore_index=True)
                            else:
                                pop_of_interest = float((data_list_old.loc[(data_list_old['ref_area.label']==str(ILO_region)+': '+str(World_Bank_Income_Group))&(data_list_old['classif1']==c)&(data_list_old['sex']==s)&(data_list_old['time']==year),['obs_value']].to_string(index=False, header=False)))
                                pop_country=pop_known_year_know*pop_of_interest/pop_total_year_know
                                from_cia_to_ilo=from_cia_to_ilo.append(pd.Series([a,cc_all.convert(names = a ,src = 'ISO3',to='name_official'),cc_all.convert(names = a ,src = 'ISO3',to='EXIO3'),s,c,year,pop_country,'ILO_region'], index=[i for i in column_data_list]),ignore_index=True)


for code in missing_data['ISO3'].values: 
    if (len(str(code)) == 3 and str(code) != 'nan'):
        print(code)
        label  = missing_data.loc[missing_data['ISO3']==code,['Label_short']]
        for s in list_sex:
            for c in classifications:
                if label.to_string(index=False, header=False) in data_list_old['ref_area.label'].values:

                    for year in range(1991,2020):
                        pop_known_year_know = float(missing_data.loc[missing_data['ISO3']==code,[year]].to_string(index=False, header=False))
                        pop_of_interest = float((data_list_old.loc[(data_list_old['ref_area.label']==label.to_string(index=False, header=False))&(data_list_old['classif1']==c)&(data_list_old['sex']==s)&(data_list_old['time']==year),['obs_value']].to_string(index=False, header=False)))
                        pop_country=pop_known_year_know*pop_of_interest/pop_total_year_know
                        from_cia_to_ilo=from_cia_to_ilo.append(pd.Series([code,cc_all.convert(names = code ,src = 'ISO3',to='name_official'),cc_all.convert(names = code ,src = 'ISO3',to='EXIO3'),s,c,year,pop_country,'ILO_Subregion_Broad'], index=[i for i in column_data_list]),ignore_index=True)
 
        
from_cia_to_ilo.to_csv('final_table_missing_countries.csv',index=False) #60 countries#


final = data_list.append(from_cia_to_ilo)
final.to_csv('complete_table.csv',index=False)

aggregation = final.groupby(['EXIO3', 'sex','classif1','time'], axis=0).sum()
aggregation.to_csv('final_table.csv')


aggregation = pd.read_csv('final_table.csv', encoding="utf-8-sig") 

'''
Create a list of the classification containing DETAILS
'''

column_names = ['Country','Sector','Mapping','Compensation of employees; wages, salaries, & employers social contributions: Low-skilled','Compensation of employees; wages, salaries, & employers social contributions: Middle-skilled','Compensation of employees; wages, salaries, & employers social contributions: High-skilled','Compensation of employees; wages, salaries, & employers social contributions: Total','ILO data /country / sector','Split','Split Low qualification employment - total','Split Middle qualification employment - total','Split High qualification employment - total','Split Low qualification employment - male','Split Middle qualification employment - male','Split High qualification employment - male','Split Low qualification employment - female','Split Middle qualification employment - female','Split High qualification employment - female']
classif_detail = [s for s in final['classif1'].unique() if ("DETAILS" in s and "TOTAL" not in s)]

'''
Creation of dataframe for salary split. One per year will be created
'''

salary_split= pd.DataFrame(columns = column_names)
concordance = pd.read_excel('aux/Exiobase_ISIC_Rev-4.xlsx', 'ILO_mapping_sector')

for code in final['EXIO3'].unique():
    for a in classif_detail:
        list_name = concordance.loc[concordance['ISIC REV 4_ILO_Alteryx']==a,['Name']]
        for b in list_name['Name']:
            salary_split=salary_split.append(pd.Series([code,b,a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], index=[i for i in column_names]),ignore_index=True)



final = final[~final['EXIO3'].str.contains("not found")]


split = {}
for years in range(1995,2019):

    for code in   final['EXIO3'].unique():
        print(code)
        print(years)
        data = '/media/ntnu/Xdrive/indecol/Projects/EXIOBASE_dev/exio3/coeff_time_series/fin/current/' + str(code) +'_' + str(years) + '.xls'
        df = pd.read_excel(data,'bpdom_fin')
        df.dropna(axis = 0, how = 'all', inplace = True)
        if (df.head(1) % 1  == 0).values.any():  # Delete first row if contains integer
            df = df.iloc[1: , :]
        df.dropna(axis = 1, how = 'all', inplace = True)
        if (df. iloc[:, 0] % 1 == 0).values.any():
            df = df.iloc[: , 1:]
        df.columns = df.iloc[0]

        for sector in salary_split['Sector'].unique():
            value_low_skill = df.loc[df[df.values=='w03.a'].index.values,[sector]].to_string(index=False, header=False)
            value_middle_skill = df.loc[df[df.values=='w03.b'].index.values,[sector]].to_string(index=False, header=False)
            value_high_skill = df.loc[df[df.values=='w03.c'].index.values,[sector]].to_string(index=False, header=False)

            salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Low-skilled']]=float(value_low_skill)/1000000
            salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Middle-skilled']]=float(value_middle_skill)/1000000
            salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: High-skilled']]=float(value_high_skill)/1000000
            salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']] = (float(value_high_skill)+float(value_middle_skill)+float(value_low_skill))/1000000
        
        for map_value in salary_split['Mapping'].unique():
            salary_split.loc[(salary_split['Country']==code)&(salary_split['Mapping']==map_value),['ILO data /country / sector']] = 1000 * aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==map_value)&(aggregation['time']==years)]['obs_value'].values[0]

        for sector in salary_split['Sector'].unique():

            salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split']]=1000*(float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']].to_string(header=False,index=False))
            )*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==concordance.loc[concordance['Name']==sector,['ISIC REV 4_ILO_Alteryx']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(salary_split.loc[(salary_split['Country'] == code)&(salary_split['Mapping'] ==concordance.loc[concordance['Name']==sector,['ISIC REV 4_ILO_Alteryx']].to_string(header=False,index=False)),['Compensation of employees; wages, salaries, & employers social contributions: Total']].sum(axis=0).values[0])
            
            
            if float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']].to_string(header=False,index=False)) != 0:
                
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - total']]=(float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split']].to_string(header=False,index=False))*float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Low-skilled']].to_string(header = False, index=False)))/float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']].to_string(header=False,index=False))
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - male']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_M')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - female']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_F')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])

                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - total']]=(float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split']].to_string(header=False,index=False))*float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Middle-skilled']].to_string(header = False, index=False)))/float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']].to_string(header=False,index=False))
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - male']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_M')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - female']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_F')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])

                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - total']]=(float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split']].to_string(header=False,index=False))*float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: High-skilled']].to_string(header = False, index=False)))/float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Compensation of employees; wages, salaries, & employers social contributions: Total']].to_string(header=False,index=False))
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - male']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_M')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - female']]=float(salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - total']].to_string(header=False,index=False))*(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_F')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])/(aggregation.loc[(aggregation['EXIO3']==code)&(aggregation['sex']=='SEX_T')&(aggregation['classif1']==salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Mapping']].to_string(header=False,index=False))&(aggregation['time']==years)]['obs_value'].values[0])

            
                
            else :
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - total']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - total']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - total']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - male']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - male']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - male']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Low qualification employment - female']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split Middle qualification employment - female']]=0
                salary_split.loc[(salary_split['Country']==code)&(salary_split['Sector']==sector),['Split High qualification employment - female']]=0

                
    split[years]=salary_split.copy()



writer = pd.ExcelWriter('split.xlsx',engine='xlsxwriter')
for year in range(1995,2019):
    split[year].to_excel(writer, sheet_name=str(year))

writer.save()


hour_list = pd.read_csv(data_path/src_csv2, encoding="utf-8-sig")
hour_list = hour_list[hour_list['classif1'].str.contains('ISIC3|ISIC4',regex=True)]
hour_list = hour_list[hour_list['time'] >1997]
hour_list = hour_list.drop(hour_list.filter(regex='note|indicator|source|obs_status').columns, axis=1)
hour_list_old=hour_list.copy()
new_table=pd.DataFrame(data=None,columns=['ref_area','sex','classif1','time','obs_value','ISIC'])

new_table_columns = new_table.columns

for code in hour_list.ref_area.unique():
    print(code)
    for sex in hour_list.sex.unique():
        for classi in hour_list.classif1.unique():
            for year in hour_list.time.unique():
                if (year<2011):#mapping ISIC4
                    if '3' in classi :
                        if not hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']].isnull().values.all(): 
                            value=hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']]
                            value = float(value.to_string(index=False, header=False))
                            new_table=new_table.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,3], index=[i for i in new_table_columns]),ignore_index=True)
                 
                else :
                    if (year>=2011): #1ere etape
                        if '4' in classi :
                            if not hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']].isnull().values.all(): 
                                value=hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']]
                                value = float(value.to_string(index=False, header=False))
                                new_table=new_table.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,4], index=[i for i in new_table_columns]),ignore_index=True)
                        else :
                            if '3' in classi: #mapping ISIC4
                                if not hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']].isnull().values.all(): 
                                    value=hour_list.loc[(hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['classif1']==classi)&(hour_list['time']==year),['obs_value']]
                                    value = float(value.to_string(index=False, header=False))
                                    new_table=new_table.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,3], index=[i for i in new_table_columns]),ignore_index=True)
                               
                                    
                              
                        #print(year,hour_list.loc[((hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['time']==year)&(hour_list['classif1']==classi)),[year]])
                    #print(code, sex, classi, year)
                    #hour_list.loc[((hour_list['ref_area']==code)&(hour_list['sex']==sex)&(hour_list['time']==year)&(hour_list['classif1']==classi)),['classif1']]=classi.translate({ord(k): None for k in digits})
            
        
        
hour_pivot = new_table.pivot(index=['ref_area','sex','classif1','ISIC'],columns='time')['obs_value']
hour_pivot.to_csv("pivot2.csv")
hour_pivot_sans_isic = new_table.pivot(index=['ref_area','sex','classif1'],columns='time')['obs_value']
hour_pivot_sans_isic.to_csv("pivot_sans_isic.csv", index=False)

hour_pivot_sans_isic_interpolate = hour_pivot_sans_isic.interpolate(method='linear',axis=1,limit_area='inside')
hour_pivot_sans_isic_interpolate.to_csv("pivot_sans_isic_interpolate.csv")


    

table_filled = regression(hour_pivot_sans_isic_interpolate)  
table_filled.to_csv("after_regression.csv")
