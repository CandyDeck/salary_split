import pandas as pd
from pathlib import Path
import ilo_download
from regression import regression
from string import digits
import string

src_url2 = "https://www.ilo.org/ilostat-files/WEB_bulk_download/indicator/HOW_TEMP_SEX_ECO_NB_A.csv.gz"
src_csv2 = Path("HOW_TEMP_SEX_ECO_NB_A.csv")

storage_root = Path(".").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"
correspondance = pd.read_excel('aux/correspondance_ISIC3_ISIC4.xls') 


hour_list = pd.read_csv(data_path/src_csv2, encoding="utf-8-sig") 

hour_list = hour_list[hour_list['classif1'].str.contains('ISIC3|ISIC4',regex=True)]
hour_list = hour_list[hour_list['sex'].str.contains('SEX_F|SEX_M',regex=True)]
hour_list = hour_list[hour_list['time'] >1997]
hour_list = hour_list.drop(hour_list.filter(regex='note|indicator|source|obs_status').columns, axis=1)
isic3=hour_list[hour_list['classif1'].str.contains('ISIC3',regex=True)].copy()
isic4=hour_list[hour_list['classif1'].str.contains('ISIC4',regex=True)].copy()

isic4_from_isic3_data=pd.DataFrame(data=None,columns=['ref_area','sex','classif1','time','obs_value'])
alphabet_string = string.ascii_uppercase
alphabet_list = list(alphabet_string)
var_holder = {}

for code in isic3.ref_area.unique():
    for year in range(1998,2021):
        for sex in isic3.sex.unique():
            for classif in isic3.classif1.unique():

                for letter in alphabet_list :
                    if classif == 'ECO_ISIC3_'+str(letter):
                        if not isic3.loc[(isic3['ref_area']==code)&(isic3['sex']==sex)&(isic3['time']==year)&(isic3['classif1']=='ECO_ISIC3_'+str(letter)),['obs_value']].isnull().values.all(): 
                            value =  float(isic3.loc[(isic3['ref_area']==code)&(isic3['sex']==sex)&(isic3['time']==year)&(isic3['classif1']==classif),['obs_value']].to_string(index=False, header=False))
                            var_holder['eco_isic3_' + str(letter.lower())] = value
                            break
                        else:
                            var_holder['eco_isic3_' + str(letter.lower())] = 0
                            break
                        
            if not (var_holder['eco_isic3_a'] == 0 and var_holder['eco_isic3_b'] ==0):
                print(year, code)
                eco_isic4_a= var_holder['eco_isic3_a'] + var_holder['eco_isic3_b']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_A',year,eco_isic4_a], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
                
            if not (var_holder['eco_isic3_c'] == 0):
                print(year, code)
                eco_isic4_b= var_holder['eco_isic3_c']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_B',year,eco_isic4_b], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
                
            if not (var_holder['eco_isic3_d'] == 0 and var_holder['eco_isic3_f'] ==0 and var_holder['eco_isic3_g'] == 0 and var_holder['eco_isic3_i'] ==0 and var_holder['eco_isic3_k'] == 0):
                print(year, code)
                eco_isic4_c= var_holder['eco_isic3_d'] + 0.05 * var_holder['eco_isic3_f'] + 0.018 * var_holder['eco_isic3_g'] + 0.029 * var_holder['eco_isic3_i'] + 0.014 * var_holder['eco_isic3_k']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_C',year,eco_isic4_c], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
                
            if not (var_holder['eco_isic3_e'] == 0):
                print(year, code)
                eco_isic4_d= 0.75 * var_holder['eco_isic3_e']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_D',year,eco_isic4_d], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_e'] == 0 and var_holder['eco_isic3_f'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_e= 0.25 * var_holder['eco_isic3_e'] + 0.05 * var_holder['eco_isic3_f'] + 0.113 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_E',year,eco_isic4_e], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
                
            if not (var_holder['eco_isic3_f'] == 0 and var_holder['eco_isic3_k'] ==0):
                print(year, code)
                eco_isic4_f= 0.85 * var_holder['eco_isic3_f'] + 0.014 * var_holder['eco_isic3_k']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_F',year,eco_isic4_f], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_g'] == 0):
                print(year, code)
                eco_isic4_g= 0.836 * var_holder['eco_isic3_g']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_G',year,eco_isic4_g], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_g'] == 0 and var_holder['eco_isic3_i'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_h= 0.018 * var_holder['eco_isic3_g'] + 0.706 * var_holder['eco_isic3_i'] + 0.019 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_H',year,eco_isic4_h], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_h'] == 0):
                print(year, code)
                eco_isic4_i=  var_holder['eco_isic3_h']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_I',year,eco_isic4_i], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_i'] == 0 and var_holder['eco_isic3_k'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_j= 0.088 * var_holder['eco_isic3_i'] + 0.233 * var_holder['eco_isic3_k'] + 0.208 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_J',year,eco_isic4_j], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_i'] == 0 and var_holder['eco_isic3_j'] ==0):
                print(year, code)
                eco_isic4_k= 0.029 * var_holder['eco_isic3_i'] + 0.926 * var_holder['eco_isic3_j']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_K',year,eco_isic4_k], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_k'] == 0 and var_holder['eco_isic3_l'] ==0):
                print(year, code)
                eco_isic4_l= 0.027 * var_holder['eco_isic3_k'] + 0.143 * var_holder['eco_isic3_l']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_L',year,eco_isic4_l], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_k'] == 0 and var_holder['eco_isic3_l'] ==0 and var_holder['eco_isic3_n'] == 0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_m= 0.26 * var_holder['eco_isic3_k'] + 0.071 * var_holder['eco_isic3_l'] + 0.077 * var_holder['eco_isic3_n'] + 0.038 * var_holder['eco_isic3_o'] 
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_M',year,eco_isic4_m], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_g'] == 0 and var_holder['eco_isic3_i'] ==0 and var_holder['eco_isic3_j'] == 0 and var_holder['eco_isic3_k'] ==0 and var_holder['eco_isic3_l'] == 0 and var_holder['eco_isic3_o'] == 0):
                print(year, code)
                eco_isic4_n= 0.018 * var_holder['eco_isic3_g'] + 0.147 * var_holder['eco_isic3_i'] + 0.037 * var_holder['eco_isic3_j'] + 0.384 * var_holder['eco_isic3_k'] + 0.071 * var_holder['eco_isic3_l']+ 0.094 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_N',year,eco_isic4_n], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_l'] == 0):
                print(year, code)
                eco_isic4_o=  0.571 * var_holder['eco_isic3_l']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_O',year,eco_isic4_o], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_k'] == 0 and var_holder['eco_isic3_m'] ==0 and var_holder['eco_isic3_n'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_p= 0.027 * var_holder['eco_isic3_k'] + var_holder['eco_isic3_m'] + 0.077 * var_holder['eco_isic3_n'] + 0.075 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_P',year,eco_isic4_p], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_l'] == 0 and var_holder['eco_isic3_n'] ==0):
                print(year, code)
                eco_isic4_q= 0.071 * var_holder['eco_isic3_l'] + 0.846 * var_holder['eco_isic3_n']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_Q',year,eco_isic4_q], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
            
            if not (var_holder['eco_isic3_k'] == 0 and var_holder['eco_isic3_l'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_r= 0.014 * var_holder['eco_isic3_k'] + 0.071 * var_holder['eco_isic3_l'] + 0.264 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_R',year,eco_isic4_r], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_g'] == 0 and var_holder['eco_isic3_j'] ==0 and var_holder['eco_isic3_k'] ==0 and var_holder['eco_isic3_o'] ==0):
                print(year, code)
                eco_isic4_s= 0.109 * var_holder['eco_isic3_g'] + 0.037 * var_holder['eco_isic3_j'] + 0.014 * var_holder['eco_isic3_k'] + 0.189 * var_holder['eco_isic3_o']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_S',year,eco_isic4_s], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_p'] == 0):
                print(year, code)
                eco_isic4_t=  var_holder['eco_isic3_p']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_T',year,eco_isic4_t], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_f'] == 0 and var_holder['eco_isic3_k'] ==0 and var_holder['eco_isic3_q'] ==0):
                print(year, code)
                eco_isic4_u= 0.05 * var_holder['eco_isic3_f'] + 0.014 * var_holder['eco_isic3_k'] +  var_holder['eco_isic3_q']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_U',year,eco_isic4_u], index=[i for i in isic4_from_isic3_data]),ignore_index=True)

            if not (var_holder['eco_isic3_x'] == 0):
                print(year, code)
                eco_isic4_x=  var_holder['eco_isic3_x']
                isic4_from_isic3_data=isic4_from_isic3_data.append(pd.Series([code,sex,'ECO_ISIC4_X',year,eco_isic4_x], index=[i for i in isic4_from_isic3_data]),ignore_index=True)
 




isic4_from_isic3_data.to_csv('isic4_from_isic3_data.csv')                        
isic4_from_isic3_data_pivot = isic4_from_isic3_data.pivot(index=['ref_area','sex','classif1'],columns='time')['obs_value']
isic4_from_isic3_data_pivot.to_csv("isic4_from_isic3_data_pivot.csv")

new_table_280121=pd.DataFrame(data=None,columns=['ref_area','sex','classif1','time','obs_value','source'])
new_table_280121_columns = new_table_280121.columns


for code in hour_list.ref_area.unique():
    print(code)
    for sex in hour_list.sex.unique():
        for classi in hour_list.classif1.unique():
            for year in hour_list.time.unique():
                if (year<2009):#mapping ISIC4

                    if not isic4_from_isic3_data.loc[(isic4_from_isic3_data['ref_area']==code)&(isic4_from_isic3_data['sex']==sex)&(isic4_from_isic3_data['classif1']==classi)&(isic4_from_isic3_data['time']==year),['obs_value']].isnull().values.all(): 
                        value=isic4_from_isic3_data.loc[(isic4_from_isic3_data['ref_area']==code)&(isic4_from_isic3_data['sex']==sex)&(isic4_from_isic3_data['classif1']==classi)&(isic4_from_isic3_data['time']==year),['obs_value']]
                        value = float(value.to_string(index=False, header=False))
                        new_table_280121=new_table_280121.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,3], index=[i for i in new_table_280121_columns]),ignore_index=True)
             
                else :
                    if (year>=2009): #1ere etape
                        if not isic4.loc[(isic4['ref_area']==code)&(isic4['sex']==sex)&(isic4['classif1']==classi)&(isic4['time']==year),['obs_value']].isnull().values.all(): 
                            value=isic4.loc[(hour_list['ref_area']==code)&(isic4['sex']==sex)&(isic4['classif1']==classi)&(isic4['time']==year),['obs_value']]
                            value = float(value.to_string(index=False, header=False))
                            new_table_280121=new_table_280121.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,4], index=[i for i in new_table_280121_columns]),ignore_index=True)
                        
                        else :
                            if isic4_from_isic3_data.loc[(isic4_from_isic3_data['ref_area']==code)&(isic4_from_isic3_data['sex']==sex)&(isic4_from_isic3_data['classif1']==classi)&(isic4_from_isic3_data['time']==year),['obs_value']].isnull().values.all(): 
                                if not isic4_from_isic3_data.loc[(isic4_from_isic3_data['ref_area']==code)&(isic4_from_isic3_data['sex']==sex)&(isic4_from_isic3_data['classif1']==classi)&(isic4_from_isic3_data['time']==year),['obs_value']].isnull().values.all(): 
                                    value=isic4_from_isic3_data.loc[(isic4_from_isic3_data['ref_area']==code)&(isic4_from_isic3_data['sex']==sex)&(isic4_from_isic3_data['classif1']==classi)&(isic4_from_isic3_data['time']==year),['obs_value']]
                                    value = float(value.to_string(index=False, header=False))
                                    new_table_280121=new_table_280121.append(pd.Series([code,sex,classi.translate({ord(k): None for k in digits}),year,value,3], index=[i for i in new_table_280121_columns]),ignore_index=True)
           
            
new_table_280121.to_csv('new_table_280121.csv')       

new_table_280121_pivot = new_table_280121.pivot(index=['ref_area','sex','classif1'],columns='time')['obs_value']
new_table_280121_pivot.to_csv("new_table_280121_pivot.csv")  


##ECO_ISIC4_A = ECO_ISIC3_A + ECO_ISIC3_B
##ECO_ISIC4_B = ECO_ISIC3_C
##ECO_ISIC4_C = ECO_ISIC3_D + 0.05 * ECO_ISIC3_F +0.018 * ECO_ISIC3_G + 0.029 * ECO_ISIC3_I +0.014 * ECO_ISIC3_K
##ECO_ISIC4_D = 0.75  * ECO_ISIC3_E
#
#ECO_ISIC4_E =  0.25 * ECO_ISIC3_E + 0.05 * ECO_ISIC3_F + 0.113 * ECO_ISIC3_O
#ECO_ISIC4_F = 0.85 * ECO_ISIC3_F + 0.014 * ECO_ISIC3_K
#ECO_ISIC4_G = 0.836 * ECO_ISIC3_G 
#ECO_ISIC4_H = 0.018 * ECO_ISIC3_G + 0.706 * ECO_ISIC3_I + 0.019 * ECO_ISIC3_O
#ECO_ISIC4_I = ECO_ISIC3_H
#ECO_ISIC4_J = 0.088 * ECO_ISIC3_I + 0.233 * ECO_ISIC3_K + 0.208 * ECO_ISIC3_O
#ECO_ISIC4_K = 0.029 * ECO_ISIC3_I + 0.926 * ECO_ISIC3_J 
#ECO_ISIC4_L = 0.027 * ECO_ISIC3_K + 0.143 * ECO_ISIC3_L
#ECO_ISIC4_M = 0.26 * ECO_ISIC3_K + 0.071 * ECO_ISIC3_L + 0.077 * ECO_ISIC3_N + 0.038 * ECO_ISIC3_O
#ECO_ISIC4_N = 0.018 * ECO_ISIC3_G + 0.147 * ECO_ISIC3_I + 0.037 * ECO_ISIC3_J + 0.384 * ECO_ISIC3_K + 0.071 * ECO_ISIC3_L + 0.094 * ECO_ISIC3_O 
#ECO_ISIC4_O = 0.571 * ECO_ISIC3_L
#ECO_ISIC4_P = 0.027 * ECO_ISIC3_K + ECO_ISIC3_M + 0.077 * ECO_ISIC3_N + 0.075 * ECO_ISIC3_O
#ECO_ISIC4_Q = 0.071 * ECO_ISIC3_L  + 0.846 * ECO_ISIC3_N 
#ECO_ISIC4_R = 0.014 * ECO_ISIC3_K + 0.071 * ECO_ISIC3_L + 0.264 * ECO_ISIC3_O
#ECO_ISIC4_S = 0.109 * ECO_ISIC3_G + 0.037 *  ECO_ISIC3_J + 0.014 * ECO_ISIC3_K + 0.189 * ECO_ISIC3_O
#ECO_ISIC4_T =  ECO_ISIC3_P
#ECO_ISIC4_U = 0.05 * ECO_ISIC3_F + 0.014 * ECO_ISIC3_K +  ECO_ISIC3_Q
#ECO_ISIC4_X =  ECO_ISIC3_X

                        
