**pop_split** is a Python 3 module written to update part of the macroeconomics data in the [EXIOBASE](https://www.exiobase.eu/) pipeline.
Data is taken from the International Labour Organization [ILO](https://www.ilo.org/global/lang--en/index.htm), a United Nations agency whose mandate is to advance social justice and promote decent work by setting international labour standards. 
The way this table is presented is shown just below. For clarity, one country (Afganisthan) and one year (2000) have been selected. We decided to show the total population allocated to each classification without distinguising the sex. However, this distinction is also provided in the ILO downloaded table.

![image.png](aux_pictures/image.png)

  The true designation of each classification is show in the table below:

![image-2.png](aux_pictures/image-2.png)

**256** ISO3 codes are available in the [country converter package](https://github.com/konstantinstadler/country_converter).

```Python
ISO3_in_EXIOBASE = cc_all.ISO3.ISO3
len(ISO3_in_EXIOBASE) = 256

ISO3_in_ILO=[item for item in list(data_list['ref_area'].unique()) if  item.isalpha()]
len(ISO3_in_ILO)=189
```

The ILO table lists **189** of them.
The missing ISO3 in the ILO table are then list in *missing_countries* :

```Python
missing_countries = []
for item in ISO3_in_EXIOBASE:
    if not item in ISO3_in_ILO:
        missing_countries.append(item)
len(missing_countries)=67
```

Part of the ISO3 code missing in ILO was find in the [CIA World Factbook](https://www.cia.gov/the-world-factbook/)
This database gives us, for an ISO3 code, the total population for a specific year.
These data are then store in a dataframe called **fetched_data**.
The table downloaded contains the differents indicators listed in the table below :




```Python
fetched_data = pd.DataFrame(data=None,columns=['ISO3','population','date'])
for a in data_cia['countries'].keys():

    name_short_cia = cc_all.convert(names = a ,src = 'regex',to='ISO3')
    print(a)
    for item in missing_countries:
        name_missing=item
        if name_missing == name_short_cia:
            print(name_missing)
            if data_cia['countries'].get(a).get('data').get('economy').get('labor_force') is not None :
                if 'total_size' in data_cia['countries'].get(a).get('data').get('economy').get('labor_force').keys():
                    print(a,data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('total_people'),data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('date'))
                    fetched_data = fetched_data.append(pd.Series([item,data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('total_people'),data_cia['countries'].get(a).get('data').get('economy').get('labor_force').get('total_size').get('date')],index=['ISO3','population','date']),ignore_index=True)
                    continue
                else:
                    continue
            else :
                continue
        else :
            continue
```

Form CIA, **43** values of total population can be retrieved. 


